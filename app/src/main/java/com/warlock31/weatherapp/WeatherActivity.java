package com.warlock31.weatherapp;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;


public class WeatherActivity extends AppCompatActivity {


    private TextView descriptionTextView;
    private TextView cityTextView;
    private TextView coordinatesTextView;
    private TextView temperatureTextView;
    private TextView cityNameTextView;
    private ImageView imageView;
    private TextView windSpeed;
    private TextView cloudText;
    private TextView humidityText;
    private TextView todaysDate;
    private ProgressDialog pd;
    private Bitmap bmp;
    protected String imageID = "";
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_weather);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setStatusBarTranslucent(true);


        //Storing the search in shared preferance for now

        saveSearchEntry(getIntent().getStringExtra("encodedCityName"));


//        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
//        //cityTextView = (TextView) findViewById(R.id.cityTextView);
//        coordinatesTextView = (TextView) findViewById(R.id.coordinatesTextView);
        temperatureTextView = (TextView) findViewById(R.id.temperatureTextView);
        cityNameTextView = (TextView) findViewById(R.id.cityNameTextView);
        imageView = (ImageView) findViewById(R.id.imageView);
        windSpeed = (TextView) findViewById(R.id.wind_speed_text);
        cloudText = (TextView) findViewById(R.id.rain_text);
        humidityText = (TextView) findViewById(R.id.humidity_text);
        todaysDate = (TextView) findViewById(R.id.todaysDate);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        temperatureTextView.setTypeface(custom_font);
        cityNameTextView.setTypeface(custom_font);
        windSpeed.setTypeface(custom_font);
        cloudText.setTypeface(custom_font);
        humidityText.setTypeface(custom_font);
        todaysDate.setTypeface(custom_font);
        try {
            String encodedCityName = getIntent().getStringExtra("encodedCityName");
            DownloadTask task = new DownloadTask();
            task.execute("http://api.openweathermap.org/data/2.5/weather?q=" + encodedCityName + "&APPID=d9c398bcfd45349083bde3f5f7e17fae");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void saveSearchEntry(String encodedCityName) {
//        Log.i("Warlock","Entered saveSearchEntry");
        sharedpreferences = getSharedPreferences(Constants.PASTSEARCHES, MODE_PRIVATE);
        String pastsearch1 = sharedpreferences.getString("pastsearch1", null);
        String pastsearch2 = sharedpreferences.getString("pastsearch2", null);
        String pastsearch3 = sharedpreferences.getString("pastsearch3", null);


        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("pastsearch1", pastsearch2);
        editor.putString("pastsearch2", pastsearch3);
        editor.putString("pastsearch3", encodedCityName);
        editor.commit();
}

public class DownloadTask extends AsyncTask<String, Void, String> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = ProgressDialog.show(WeatherActivity.this, "", "Please Wait", false);
    }

    @Override
    protected String doInBackground(String... urls) {

        String result = "";
        URL url;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL(urls[0]);

            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = urlConnection.getInputStream();

            InputStreamReader reader = new InputStreamReader(in);

            int data = reader.read();

            while (data != -1) {

                char current = (char) data;

                result += current;

                data = reader.read();

            }

            return result;

        } catch (Exception e) {

            Toast.makeText(getApplicationContext(), "Could not find weather", Toast.LENGTH_LONG);

        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        pd.dismiss();

        try {

            String message = "";


            JSONObject jsonObject = new JSONObject(result);

            String weatherInfo = jsonObject.getString("weather");

            Log.i("Weather content", weatherInfo);

            JSONArray arr = new JSONArray(weatherInfo);

            for (int i = 0; i < arr.length(); i++) {

                JSONObject jsonPart = arr.getJSONObject(i);

                String main = "";
                String description = "";

                main = jsonPart.getString("main");
                description = jsonPart.getString("description");
                imageID = jsonPart.getString("icon");

                if (main != "" && description != "") {

                    message += main + ": " + description + "\r\n";

                }

            }

            //For Getting City Name
            String cityNameInfo = jsonObject.getString("name");
            if (cityNameInfo != "") {

                cityNameTextView.setText(cityNameInfo.toUpperCase());

            } else {
                cityNameTextView.setText("NA");
            }

            //For getting Speed

            JSONObject windSpeedObject = jsonObject.getJSONObject("wind");
            windSpeed.setText(windSpeedObject.getString("speed") + " m/s");


            //For Getting Cloudy

            JSONObject cloudObject = jsonObject.getJSONObject("clouds");
            cloudText.setText(cloudObject.getString("all") + "%");


            //For getting the temperature
            JSONObject temperatureInfo = jsonObject.getJSONObject("main");
            Log.i("Temperature Content", temperatureInfo.toString());
            String temp = temperatureInfo.getString("temp");
            double tempTemperature = Double.valueOf(temp);
            double newTemperature = tempTemperature - 273.15;
            if (String.valueOf(newTemperature) != "") {

                temperatureTextView.setText(String.format("%.0f", newTemperature) + "°");

            } else {
                temperatureTextView.setText("NA");
            }

            humidityText.setText(temperatureInfo.getString("humidity") + "%");
            //Log.i("Temperature Content",String.valueOf(newTemperature));

            long date = System.currentTimeMillis();

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
            String dateString = sdf.format(date);
            todaysDate.setText(dateString);
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
//                            URL imageUrl = "http://openweathermap.org/img/w/".imageID.".png";
                        InputStream in = new URL("http://openweathermap.org/img/w/" + imageID + ".png").openStream();
                        bmp = BitmapFactory.decodeStream(in);
                    } catch (Exception e) {
                        // log error
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    if (bmp != null)
                        imageView.setImageBitmap(bmp);
                }

            }.execute();

        } catch (JSONException e) {

            Toast.makeText(getApplicationContext(), "Could not find weather", Toast.LENGTH_LONG);

        }


    }

}

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


}
