package com.warlock31.weatherapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {


    EditText cityName;
    TextView cityTextView;
    TextView pastSearchMainText;
    TextView pastSearch1;
    TextView pastSearch2;
    TextView pastSearch3;
    TextView emptySearch;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCY7JPOg-CsjjuBr6jNSWQ3eWXWCOChJug";
    ProgressDialog pd;

    public void getWeather(View view) {

//        Log.i("Warlock", "Entered getWeather");
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(cityName.getWindowToken(), 0);
        if (cityName.getText().length() > 0) {
            Intent intent = new Intent(getApplicationContext(), WeatherActivity.class);
            try {
                String encodedCityName = URLEncoder.encode(cityName.getText().toString(), "UTF-8");
                intent.putExtra("encodedCityName", encodedCityName);
                if (isNetworkAvailable(getApplicationContext())) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please check you internet connection", Toast.LENGTH_LONG).show();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please enter the city name", Toast.LENGTH_LONG).show();
        }
    }


    public void getWeatherFromPastSearches(View view,String cityName){

        if (cityName.length() > 0) {
            Intent intent = new Intent(getApplicationContext(), WeatherActivity.class);
            try {
                String encodedCityName = URLEncoder.encode(cityName, "UTF-8");
                intent.putExtra("encodedCityName", encodedCityName);
                if (isNetworkAvailable(getApplicationContext())) {
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please check you internet connection", Toast.LENGTH_LONG).show();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please enter the city name", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTranslucent(true);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        cityName = (EditText) findViewById(R.id.autoCompleteTextView);
        cityTextView = (TextView) findViewById(R.id.textView);
        pastSearchMainText = (TextView) findViewById(R.id.textView2);
        pastSearch1 = (TextView) findViewById(R.id.firstCitySearch);
        pastSearch2 = (TextView) findViewById(R.id.secondCitySearch);
        pastSearch3 = (TextView) findViewById(R.id.thirdCitySearch);
        emptySearch = (TextView) findViewById(R.id.emptyCitySearch);

        pastSearch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeatherFromPastSearches(view,pastSearch1.getText().toString());
            }
        });

        pastSearch2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeatherFromPastSearches(view,pastSearch3.getText().toString());
            }
        });

        pastSearch3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWeatherFromPastSearches(view,pastSearch3.getText().toString());
            }
        });

        setPastSearched();

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans-Regular.ttf");
        cityName.setTypeface(custom_font);
        cityTextView.setTypeface(custom_font);
        pastSearchMainText.setTypeface(custom_font);
        pastSearch1.setTypeface(custom_font);
        pastSearch2.setTypeface(custom_font);
        pastSearch3.setTypeface(custom_font);
        emptySearch.setTypeface(custom_font);
        cityName.setFocusableInTouchMode(true);
        cityName.requestFocus();
        cityName.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (i == KeyEvent.KEYCODE_ENTER && keyEvent.getAction()==KeyEvent.ACTION_DOWN) {
//                    Toast.makeText(getApplicationContext(),"Yeyyy",Toast.LENGTH_LONG).show();
                    Log.i("Warlock","Enter Pressed");
                    getWeather(view);
                    Log.i("Warlock","getWeather Executed");
                    return true;
//
                }
                return false;
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setPastSearched();
    }

    protected void setPastSearched() {
//        Log.i("Warlock","Entered setPastSearched");
        SharedPreferences sharedpreferences = getSharedPreferences(Constants.PASTSEARCHES, MODE_PRIVATE);
        String pastsearchString1 = sharedpreferences.getString("pastsearch1", "");
        String pastsearchString2 = sharedpreferences.getString("pastsearch2", "");
        String pastsearchString3 = sharedpreferences.getString("pastsearch3", "");
//        Log.i("Warlock",pastsearchString1 + pastsearchString2 + pastsearchString3);
        if (pastsearchString1.isEmpty()){
            emptySearch.setText("No Past Searches");
        }else{
            pastSearch1.setText(pastsearchString3.toUpperCase());
            pastSearch2.setText(pastsearchString2.toUpperCase());
            pastSearch3.setText(pastsearchString1.toUpperCase());
        }



    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


}
